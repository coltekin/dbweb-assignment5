<?php
error_reporting(-1);
ini_set("display_errors", 1);
session_name('VOTE_SESSION');
session_start();

try {
    $dbh = new PDO('sqlite:./vote.db');
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    echo "Database error: ". $e->getMessage();
    die();
}


if (isset($_REQUEST['activate'])) {
    $q = "update user set active = 1 where uname='"
         . $_REQUEST['activate'] . "';";
    $dbh->exec($q);
} else if(isset($_REQUEST['nominate'])) {
    $q = "update user set candidate = 1 where uname='"
         . $_REQUEST['nominate'] . "';";
    $dbh->exec($q);
} else if(isset($_REQUEST['cancel'])) {
    $q = "update user set candidate = 0 where uname='"
         . $_REQUEST['cancel'] . "';";
    $dbh->exec($q);
}

$q = "select count(*) from user where active = 0";

if(current($dbh->query($q)->fetch()) > 0) {
    $q = "select uname from user where active = 0";
    $qh = $dbh->query($q);
    echo '<p> Non active users';
    echo '<form action="" method="post">';
    echo '<table>';
    while($row = $qh->fetch()) {
        echo '<tr><td>' . $row['uname'];
        echo '<td><button type="submit" name="activate" value="'
            . $row['uname'] . '"> Activate </button><br>';
    }
    echo '</table></form>';
} else {
    echo "There is no pending user activation approvals.";
}

echo "<p>Active users:";
$q = "select uname,candidate from user where active = 1";
$qh = $dbh->query($q);
echo '<form action="" method="post">';
echo '<table>';
while($row = $qh->fetch()) {
    echo '<tr><td>' . $row['uname'];
    if ($row['candidate'] == 0) {
        echo '<td><button type="submit" name="nominate" value="'
            . $row['uname'] . '"> Nominate </button><br>';
    } else {
        echo '<td><button type="submit" name="cancel" value="'
            . $row['uname'] . '"> Cancel nomination </button><br>';
    }
}
echo '</table></form>';


?>
<p>We assume we have a full administrative interface to add/remove users 
as well making them administrators.
The above is enough for the exercise. Not having an extensive interface
saves your and my time :)

<p>Now you can return to the <a href="index.php">voting system</a>.
