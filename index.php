<?php
error_reporting(-1);
ini_set("display_errors", 1);
session_name('VOTE_SESSION');
session_start();

$self = $_SERVER['PHP_SELF'];

if (isset($_SESSION['state'])) {
    try {
        $dbh = new PDO('sqlite:./vote.db');
        $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo "Database error: ". $e->getMessage();
        die();
    }
} else {
    $_SESSION['state'] = 'login';
}

$registered = False;

if ($_SESSION['state'] == 'login') {
    if(isset($_REQUEST['uname'])) {
        $q =  "select passwd from user where uname = '"
              . $_REQUEST['uname'] . "';";
        $qh = $dbh->query($q);
        $passwd = $qh->fetch()['passwd'];
        if ($passwd) {
            if ($passwd == $_REQUEST['passwd']) {
                $q =  'select active, `admin`, candidate from user'
                    . " where uname = '" . $_REQUEST['uname'] . "';";
                $qh = $dbh->query($q);
                $row =  $qh->fetch();
                if ($row['active'] != "1") {
                    echo "<p>Your account is not activated yet.\n";
                    echo "Please try later.</p>";
                } else {
                    $candidate = $row['candidate'];
                    $admin = $row['admin'];
                    $_SESSION['state'] = 'logged_in';
                    $_SESSION['user'] = $_REQUEST['uname'];
                    $_SESSION['admin'] = $admin;
                }
            } else {
                echo "<p>Wrong password.</p>";
            }
        } else {
            echo "<p>Unknown user.</p>";
        }
    } else if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'register') {
        $_SESSION['state'] = 'register';
    }
} 

if ($_SESSION['state'] == 'register') {
    if(isset($_REQUEST['uname'])) {
        $q = "insert into user(uname, passwd) values('"
            . $_REQUEST['uname'] ."','" 
            . $_REQUEST['passwd'] ."');";
        if (! $dbh->exec($q)) {
            echo '<p>The user name is taken, please pick another one.</p>';
            $registered = False;
        } else {
            $registered = True;
        }
    }
} 
if ($_SESSION['state'] == 'logged_in') {
    if(isset($_REQUEST['page'])){
        if($_REQUEST['page'] == 'logout') {
            echo 'goodbye!<br>';
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            session_destroy();
            exit(0);
        }
    } else {
        $q = "select candidate from votes where voter = '"
             .  $_SESSION['user']. "';";
        $qh = $dbh->query($q);
        if ($row = $qh->fetch()) {
            $current_vote = $row['candidate'];
        }

        if (isset($_REQUEST['vote'])) {
            if (isset($current_vote)) {
                $q = "update votes set candidate = '" . $_REQUEST['vote'] 
                    . "', vote_time = datetime('now') "
                    . "where voter = '" . $_SESSION['user'] . "';";
            } else {
                $q = "insert into votes values ('"
                    . $_SESSION['user'] ."', '". $_REQUEST['vote'] 
                    . "', datetime('now'));";
            }
            $dbh->exec($q);
            $current_vote = $_REQUEST['vote'];
        }

        $q = 'select uname from user where candidate = 1;';
        $qh = $dbh->query($q);
        $vote = array();
        while ($c = $qh->fetch()['uname']) {
            $vote[$c] = 0;
        }

        $q = 'select candidate, count(candidate) as votec from votes '
            .'group by candidate;';
        $qh = $dbh->query($q);
        while ($row = $qh->fetch()) {
            $c = $row['candidate'];
            $v = $row['votec'];
            $vote[$c] = $v;
        }
    }
}


if ($_SESSION['state'] == 'login'){
        echo '<p>Please log in.';
        echo '<p><form action="'.$self.'" method="post"><br>';
        echo 'Username: <input type="text" name="uname"><br>';
        echo 'Password: <input type="password" name="passwd""><br>';
        echo '<input type="submit" name="goto" value="login">';
        echo ' or <a href="'.$self.'?page=register">register</a>.<br>';
        echo '</form></p>';
} else if ($_SESSION['state'] == 'register'){
    if (!$registered) {
        $passwd="";
        if(isset($_REQUEST['passwd'])) $passwd = $_REQUEST['passwd'];
        echo '<p><form action="'.$self.'" method="post"><br>';
        echo 'Username: <input type="text" name="uname"><br>';
        echo 'Password: <input type="password" name="passwd" value="'
            . $passwd . '"><br>';
        echo '<input type="submit" name="register" value="register">';
        echo '</form></p>';
    } else {
        echo '<p>Your registration is pending approval.</p>';
        echo '<p>Please return later to login and vote.</p>';
        $_SESSION['state'] = 'login';
    }
} else if ($_SESSION['state'] == 'logged_in'){
    echo '<p>You can vote or change your vote below.';
    echo '<p><form action="'.$self.'" method="post"><br>';
    foreach ($vote as $c => $nvotes) {
        if (isset($current_vote) && $c == $current_vote) {
            echo '<input type="radio" name="vote" value="'.$c.'" checked>';
        } else {
            echo '<input type="radio" name="vote" value="'.$c.'">';
        }
        echo $c .' ('. $nvotes .' votes)<br>';
    }
    echo '<input type="submit" name="submitvote" value="Vote">';
    echo '</form></p>';
    echo '<p>You can logout <a href="'.$self.'?page=logout">here</a>.';
    if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) {
        echo 'Or switch to the administrative interface ';  
        echo '<a href="admin.php">here</a>.';
    }
}

?>
