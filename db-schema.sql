create table user(uname varchar(50), 
                  passwd varchar(50), 
                  active int default 0,
                  admin int default 0, 
                  candidate int default 0, 
                  primary key(uname));

create table votes(voter varchar(50),
                   candidate varchar(50),
                   vote_time datetime,
                   primary key(voter, candidate)
                   foreign key(voter) references user(uname),
                   foreign key(candidate) references user(uname));
insert into user(uname, passwd, active, admin) values('admin', 'admin', 1, 1);
